const containerIcon = document.querySelector('.nav__icon');
const menu = document.querySelector('.menu');
const icons = document.querySelectorAll('.icon');
const menuItems = document.querySelectorAll('.menu__item');

containerIcon.addEventListener('click', function () {
    menu.classList.toggle('menu--display');
    icons.forEach((icon) => {
        icon.classList.toggle('icon--display');
    })
})

